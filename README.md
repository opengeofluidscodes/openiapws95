# README #

OpenIAPWS95 is a C++ implementation of the IAPWS95 equation of state. The current implementation covers the ideal gas and residual contributions to the Helmholtz Free energy as well as some thermodynamic properties as well as saturation properties.

This is version 0.1.

Most functions such as the Helmholtz free energy contributions and thermodynamic properties that derive from it, such as pressure, enthalpy etc. have been tested against values in the official IAPWS release (see http://www.iapws.org/relguide/IAPWS-95.html) as well as against data obtained from the NIST web book interface (http://webbook.nist.gov/chemistry/fluid/). The RhoFromTP function has not rigorously been tested. 

Disclaimer: there is no legal or other guarantee that the code will perform correctly.

Zurich, September 2016, Thomas Driesner

Update October 2018. While the original functionality performed well, saturation pressure near the critical poitn did not converge within 10^-6. I am in the process of testing - and hopefully improving - this ...

### How do I get it running? ###

I provide no installation instructions etc. at this stage. Just clone and compile it, and try ... If you can't read simple C++, this is nothing for you.

### Contribution guidelines ###

If you are interested in participating in this project (namely in writing nicer code, tests, documentation etc.), please contact me under thomas.driesner@erdw.ethz.ch . If I don't respond in due time, please send a reminder.

P.S.
There are no plans at this stage to provide source code for the CSMP++ version of my H2O-NaCl model published in 2007.