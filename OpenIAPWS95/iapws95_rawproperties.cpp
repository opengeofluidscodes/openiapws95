/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "iapws95_rawproperties.h"
#include "iapws95_critical_constants.h"

IAPWS95_Rawproperties::IAPWS95_Rawproperties():r(0.46151805)
{
    
}
        

double IAPWS95_Rawproperties::Pressure( const double& rho, const double& TK )
{
  const double delta{ rho/criticalDensity };
  return (1.0+delta*eos_residualpart.NondimensionalDResidualPartDdelta( rho, TK ))*rho*r*TK*1.0e3;
}

double IAPWS95_Rawproperties::InternalEnergy( const double& rho, const double& TK )
{
  const double tau( criticalTemperatureInKelvin/TK );
  return tau*r*TK*1.0e3*( eos_idealgaspart.NondimensionalDIdealGasPartDtau(TK)
                          + eos_residualpart.NondimensionalDResidualPartDtau(rho, TK) );
}


double IAPWS95_Rawproperties::Entropy( const double& rho, const double& TK )
{
  const double tau( criticalTemperatureInKelvin/TK );
  double entropy = tau*( eos_idealgaspart.NondimensionalDIdealGasPartDtau(TK)
                         + eos_residualpart.NondimensionalDResidualPartDtau(rho, TK) );
  entropy -= eos_idealgaspart.NondimensionalIdealGasPart( rho, TK );
  entropy -= eos_residualpart.NondimensionalResidualPart( rho, TK );
  entropy *= r*1.0e3;
  return entropy;
}


double IAPWS95_Rawproperties::Enthalpy( const double& rho, const double& TK )
{
  const double delta( rho/criticalDensity );
  const double tau( criticalTemperatureInKelvin/TK );
  double enthalpy( 1.0 + tau*( eos_idealgaspart.NondimensionalDIdealGasPartDtau(TK)
                               + eos_residualpart.NondimensionalDResidualPartDtau(rho, TK) ) );
  enthalpy += delta*eos_residualpart.NondimensionalDResidualPartDdelta( rho, TK);
  return enthalpy*r*TK*1.0e3;
}


double IAPWS95_Rawproperties::IsochoricHeatCapacity( const double& rho, const double& TK )
{
  const double tau( criticalTemperatureInKelvin/TK );
  return -tau*tau*r*(eos_idealgaspart.NondimensionalD2IdealGasPartDtau2(TK)
                     + eos_residualpart.NondimensionalD2ResidualPartDtau2(rho,TK));
}


double IAPWS95_Rawproperties::IsobaricHeatCapacity( const double& rho, const double& TK )
{
  const double delta( rho/criticalDensity );
  const double tau( criticalTemperatureInKelvin/TK );
  double cp(IsochoricHeatCapacity(rho,TK));
  double term1(1.0 + delta*eos_residualpart.NondimensionalDResidualPartDdelta(rho,TK) 
               - delta*tau*eos_residualpart.NondimensionalDResidualPartDdeltaDtau(rho,TK));
  double term2(1.0 + 2.0*delta*eos_residualpart.NondimensionalDResidualPartDdelta(rho,TK) 
               + delta*delta*eos_residualpart.NondimensionalD2ResidualPartDdelta2(rho,TK));
  return cp+r*term1*term1/term2;
}
