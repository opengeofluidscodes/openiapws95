/*
 * Copyright (C) 2018 td
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   iapws95_eos_residualpart.h
 * Author: td
 *
 * Created on October 19, 2018, 12:56 PM
 */

#ifndef IAPWS95_EOS_RESIDUALPART_H
#define IAPWS95_EOS_RESIDUALPART_H

#include<array>

class IAPWS95_ResidualPart{

 private:

  const double tcrit;
  const double rhocrit;
  const std::array<int,51>    c;
  const std::array<int,54>    d;
  const std::array<double,54> t;
  const std::array<double,56> n;
  const std::array<double,3>  alfa;
  const std::array<double,5>  beta;
  const std::array<double,3>  gamma;
  const std::array<double,3>  epsilon;
  const std::array<double,2>  C;
  const std::array<double,2>  D;
  const std::array<double,2>  A;
  const std::array<double,2>  B;
  const std::array<double,2>  b;
  const std::array<double,2>  a;
    
 public:

  IAPWS95_ResidualPart();
  ~IAPWS95_ResidualPart();
  
  // Re-arranging functionality
  
  // 1. Core functions of e.o.s.
  double NondimensionalResidualPart(const double& rho, const double& TK);
    
  // 2. First derivatives of core functions
  double NondimensionalDResidualPartDdelta(const double& rho, const double& TK);
  double NondimensionalDResidualPartDtau(const double& rho, const double& TK);
  
  // 3. Second and mixed derivatives of core functions
  double NondimensionalD2ResidualPartDdelta2(const double& rho, const double& TK);
  double NondimensionalD2ResidualPartDtau2(const double& rho, const double& TK);
  double NondimensionalDResidualPartDdeltaDtau(const double& rho, const double& TK);

};

#endif /* IAPWS95_EOS_RESIDUALPART_H */

