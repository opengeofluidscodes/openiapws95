///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//// --- Saturation property calculations ---
//
//
//
//double iapws95::SaturationPressure( const double& TK )
//{
//  double rhop(EstimateSaturationLiquidDensity( TK ));
//  double rhopp(EstimateSaturationVaporDensity( TK )); 
//  double tau(tcrit/TK); 
//  double deltap(rhop/rhocrit); 
//  double deltapp(rhopp/rhocrit);
//  double pS  = EstimateSaturationPressure(TK); //MPa
//  double pS0 = pS;
//  
//  double resid(1.0);
//  int    counter(0);
//
//  cout << "estimated : " << pS << endl;
//
//  //return r*1000.*TK*(ResidualPart(rhop,TK)-ResidualPart(rhopp,TK)+log(rhop/rhopp))/(1./rhopp-1./rhop);
//  
////  Junglas' version
////  if( TK <= tcrit - 3.5e-3)
////  {
////      {
////          ++counter;
////          pS    = r*1000.*TK*(ResidualPart(rhop,TK)-ResidualPart(rhopp,TK)+log(rhop/rhopp))/(1./rhopp-1./rhop);
////          rhop  = Density(TK,pS,rhop);
////          rhopp = Density(TK,pS,rhopp);
////          resid = fabs((pS-pS0)/pS);
////          pS0   = pS;
////          cout << pS << "\t" << pS0 << endl;
////          cout << counter << "\t" << pS << "\t" << rhop << "\t" << rhopp << endl;
////          if(counter > 10) break;
////      }
////  }
////  else if( T <= tcrit - 6.0e-6)
////  {
////      
////  }
////  else
////  {
////      
////  }
//// This follows the procedure outlined
//// by Shi&Mao(2012), Geoscience Frontiers, 3, 51-58 //  while(resid > 1.0e-14)
////    {
////      deltap  += 1.0/Lambda(tau,deltap,deltapp)
////        *( ( K(tau,deltapp)-K(tau,deltap) ) * Jdelta(tau,deltapp) 
////           - 
////           ( J(tau,deltapp)-J(tau,deltap) )*Kdelta(tau,deltapp) );
////
////      deltapp += 1.0/Lambda(tau,deltap,deltapp)
////        *( ( K(tau,deltapp)-K(tau,deltap) ) * Jdelta(tau,deltap) 
////           - 
////           ( J(tau,deltapp)-J(tau,deltap) )*Kdelta(tau,deltap) );
////      rhop  = deltap*rhocrit;   
////      rhopp = deltapp*rhocrit;
////      resid = fabs(Pressure( rhop, TK ) - Pressure( rhopp, TK ));//fabs( K(tau,deltapp)-K(tau,deltap) ) + fabs( J(tau,deltapp)-J(tau,deltap) );
////      ++counter;
////      cout << counter << "\t" << resid << endl;
////      if(counter > 10000) 
////      {
////          cerr << "double iapws95::SaturationPressure( const double& TK )\n";
////          cerr << "did not converge within " << counter << " iterations.\n";
////          break;
////      }
////    }
////  cout << "Pressure( rhop, TK ) - Pressure( rhopp, TK ) = " << Pressure( rhop, TK ) - Pressure( rhopp, TK ) << endl;
////  return 0.5*( Pressure( rhop, TK ) + Pressure( rhopp, TK ) );
//}
//
//double iapws95::SaturationTemperature( const double& Ppa )
//{
//  double pmax(22064000.00000213);
//  double pmin(611.655);
//  double tK{};
//  double p{};
//  double tmin = 273.16;
//  double tmax = 647.096;
//  // Implement range check
//
//  double resid(1.);
//  int count(0);
//  while(fabs(resid) > 1.0e-6)
//    {
//        ++count;
//        tK = 0.5*(tmin+tmax);
//        p = SaturationPressure( tK );
//        resid = p-Ppa;
//        if( p < Ppa ) tmin=tK;
//        else if(p > Ppa) tmax = tK;
//        else return tK;
//        if(count > 100) break;
//    }
//  return tK;
//}
//
//
//double iapws95::LiquidSaturationDensity(  const double& TK )
//{
//  // This follows the procedure outlined
//  // by Shi&Mao(2012), Geoscience Frontiers, 3, 51-58 
//  double rhop(EstimateSaturationLiquidDensity( TK ));
//  double rhopp(EstimateSaturationVaporDensity( TK )); 
//  double tau(tcrit/TK); 
//  double deltap(rhop/rhocrit); 
//  double deltapp(rhopp/rhocrit);
//
//  double resid(1.0);
//  int    counter(0);
//
//  while(resid > 1.0e-9)
//    {
//      deltap  += 1.0/Lambda(tau,deltap,deltapp)
//        *( ( K(tau,deltapp)-K(tau,deltap) ) * Jdelta(tau,deltapp) 
//           - 
//           ( J(tau,deltapp)-J(tau,deltap) )*Kdelta(tau,deltapp) );
//
//      deltapp += 1.0/Lambda(tau,deltap,deltapp)
//        *( ( K(tau,deltapp)-K(tau,deltap) ) * Jdelta(tau,deltap) 
//           - 
//           ( J(tau,deltapp)-J(tau,deltap) )*Kdelta(tau,deltap) );
//
//      resid = fabs( K(tau,deltapp)-K(tau,deltap) ) + fabs( J(tau,deltapp)-J(tau,deltap) );
//      ++counter;
//      if(counter > 20) break;
//    }
//  return deltap*rhocrit;
//}
//
//double iapws95::VaporSaturationDensity(  const double& TK )
//{
//  // This follows the procedure outlined
//  // by Shi&Mao(2012), Geoscience Frontiers, 3, 51-58 
//  double rhop(EstimateSaturationLiquidDensity( TK ));
//  double rhopp(EstimateSaturationVaporDensity( TK )); 
//  double tau(tcrit/TK); 
//  double deltap(rhop/rhocrit); 
//  double deltapp(rhopp/rhocrit);
//
//  double resid(1.0);
//  int    counter(0);
//
//  while(resid > 1.0e-9)
//    {
//      deltap  += 1.0/Lambda(tau,deltap,deltapp)
//        *( ( K(tau,deltapp)-K(tau,deltap) ) * Jdelta(tau,deltapp) 
//           - 
//           ( J(tau,deltapp)-J(tau,deltap) )*Kdelta(tau,deltapp) );
//
//      deltapp += 1.0/Lambda(tau,deltap,deltapp)
//        *( ( K(tau,deltapp)-K(tau,deltap) ) * Jdelta(tau,deltap) 
//           - 
//           ( J(tau,deltapp)-J(tau,deltap) )*Kdelta(tau,deltap) );
//
//      resid = fabs( K(tau,deltapp)-K(tau,deltap) ) + fabs( J(tau,deltapp)-J(tau,deltap) );
//      ++counter;
//      if(counter > 20) break;
//    }
//  return deltapp*rhocrit;
//}
//
//
//double iapws95::LiquidSaturationDensityForP ( const double& Ppa )
//{
//    double TK = SaturationTemperature( Ppa );
//    return LiquidSaturationDensity( TK );
//}
//
//double iapws95::LiquidSaturationMolarVolumeForP ( const double& Ppa )
//{
//    return DensityAndX2MolarVolume ( LiquidSaturationDensityForP( Ppa ), 0.0 );
//}
//
//
//
//double iapws95::J( const double& tau, const double& delta )
//{
//  double TK(tcrit/tau);
//  double rho(delta*rhocrit);
//  return delta*( 1.0 + delta*DResidualPartDdelta( rho, TK ));
//}
//
//double iapws95::K( const double& tau, const double& delta )
//{
//  double TK(tcrit/tau);
//  double rho(delta*rhocrit);
//  return delta*DResidualPartDdelta( rho, TK )
//    + ResidualPart( rho, TK )
//    + log(delta);
//}
//
//double iapws95::Jdelta( const double& tau, const double& delta )
//{
//  double TK(tcrit/tau);
//  double rho(delta*rhocrit);
//  return 1.0 + 2.0*delta*DResidualPartDdelta( rho, TK ) + delta*delta*D2ResidualPartDdelta2( rho, TK );
//}
//
//double iapws95::Kdelta( const double& tau, const double& delta )
//{
//  double TK(tcrit/tau);
//  double rho(delta*rhocrit);
//  return 2.0*DResidualPartDdelta( rho, TK ) + delta*D2ResidualPartDdelta2( rho, TK ) + 1.0/delta;
//}
//
//double iapws95::Lambda( const double& tau,
//                        const double& deltap, const double& deltapp )
//{
//  return Jdelta(tau,deltapp) * Kdelta(tau,deltap) - Jdelta(tau,deltap) * Kdelta(tau,deltapp); 
//}
//
