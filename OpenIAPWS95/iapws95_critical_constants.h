/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   iapws95_critical_constants.h
 * Author: td
 *
 * Created on October 4, 2018, 9:35 AM
 */

#ifndef IAPWS95_CRITICAL_CONSTANTS_H
#define IAPWS95_CRITICAL_CONSTANTS_H

const double criticalTemperatureInKelvin{647.096}; // K
const double criticalDensity{322.0}; // kg m-3

#endif /* IAPWS95_CRITICAL_CONSTANTS_H */

