/*
 * Copyright (C) 2018 td
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "iapws95_TP_interface.h"


IAPWS95_TP_Interface::IAPWS95_TP_Interface( const double& tKelvin, const double& pPascal )
:
tKelvin(tKelvin),
pPascal(pPascal),
density(tKelvin,pPascal)
{
    
}

// This will be the same pattern for all calls - how to generalize, keeping a simple syntax?
double IAPWS95_TP_Interface::InternalEnergy()
{
    double rho = density.DensityFromTP();
    return raw.InternalEnergy( rho, tKelvin );
}

double IAPWS95_TP_Interface::Entropy()
{
    double rho = density.DensityFromTP();
    return raw.Entropy( rho, tKelvin );
}

double IAPWS95_TP_Interface::Enthalpy()
{
    double rho = density.DensityFromTP();
    return raw.Enthalpy( rho, tKelvin );
}

double IAPWS95_TP_Interface::IsochoricHeatCapacity()
{
    double rho = density.DensityFromTP();
    return raw.IsochoricHeatCapacity( rho, tKelvin );
}

double IAPWS95_TP_Interface::IsobaricHeatCapacity()
{
    double rho = density.DensityFromTP();
    return raw.IsobaricHeatCapacity( rho, tKelvin );
}

