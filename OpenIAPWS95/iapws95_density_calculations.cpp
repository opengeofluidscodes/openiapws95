/*
 * Copyright (C) 2018 td
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * IAPWS95_TP_Interface::DensityFromTP
 * 
 * Convergence is found to be poor very close to saturation pressure, also in the immediate supercritical region
 * An improved (and fast?) iteration scheme is highly welcome 
 * 
 * obsolete(?): rho0 is defaulted to 500. Then, the internal routine as programmed will be used.
 * If it is to be overridden, e.g., to enforce vapor-like density, specify a lower value
 */
#include "iapws95_density_calculations.h"
#include "iapws95_critical_constants.h"
#include "iapws95_auxfunctions.h"

#include <boost/math/tools/roots.hpp>

#include <limits>
#include <iostream>

using namespace std;

IAPWS95_DensityCalculations::IAPWS95_DensityCalculations( const double& tKelvin, const double& pPascal )
: tKelvin(tKelvin),pPascal(pPascal),r(0.46151805), rho0(1000.), bracket_factor(1.005)
{
    
}

// as a start, this function mimics the MATLAB water95 functionality
void IAPWS95_DensityCalculations::FindInitialDensityAndBrackets()
{
    if(tKelvin >= criticalTemperatureInKelvin){ rho0 = 600.; bracket_factor = 1.1; }
    else
    {
        double approximateSaturationPressure = EstimateSaturationPressure(tKelvin);
        if( pPascal > 1.00005*approximateSaturationPressure){
            cout << "1\n";
            if(pPascal < 13.7263594135030e5*tKelvin - 7130.8669147992003e5){rho0 = 800.; bracket_factor = 1.01;}
            else{rho0 = 1000.; bracket_factor = 1.01;}
        }
        else if(pPascal < 0.9999*approximateSaturationPressure)
        { 
            cout << "2\n";
            if(pPascal < 5.0e4){rho0 = 0.1; bracket_factor = 1.1; }
            else if(pPascal > 100.0e5){rho0 = 100.; bracket_factor = 1.01; }
            else{rho0 = 1.0; bracket_factor = 1.05; }
        }
        else
        {
            cout << "3\n";
        }
    }
    //22.06400000000213e6
}

double IAPWS95_DensityCalculations::DensityFromTP()
{
    // Prepare function arguments of boost's bracket_and_solve_root
    IAPWS95_PressureDifferenceFunctor pressureDifference(tKelvin,pPascal);
    FindInitialDensityAndBrackets();
    std::pair<double,double>           iteration_result;
    uint digits = std::numeric_limits<double>::digits;
    boost::math::tools::eps_tolerance<double> mytol(digits-2);
    boost::uintmax_t max_iter{1000};

    // run boost's bracket_and_solve_root
    iteration_result = boost::math::tools::bracket_and_solve_root(pressureDifference,rho0,bracket_factor,true,mytol,max_iter);

    // evaluate boost's bracket_and_solve_root run and return
    if(mytol(iteration_result.first,iteration_result.second)==true) cout << "Converged!\n";
    else cout << "max_iter exceeded\n";
    return 0.5*(iteration_result.first+iteration_result.second);

}




    
// old stuff, don't throw away yet    
//    double rhomin{}, rhomax{}, rho{};
//    int maxcount{1000};
//    bool printinfo{false};     
    //  if( TK >= tcrit )
//    {
//      rhomin = 1.0e-10;
//      rhomax = 1300.;
//      rho   = 500.;
//    }
//  else
//    {
//      psat = SaturationPressure(TK);
//      if( rho0 < rhocrit)
//        {
//          rhomin = 1.0e-10;
//          rhomax = VaporSaturationDensity( TK );
//          rho   = rhomax*0.95;
//        }
//      else
//        { 
//          rhomin = LiquidSaturationDensity( TK );
//          rhomax = 1300.;
//          rho   = rhomin*1.05;
//        }
//    }
//  double diff = 1.0;
//  int counter(0);
//  while( fabs(diff) > 1.0e-7 )
//    {
//      ++counter;
//      diff = Pressure( rho, TK ) - Ppa;
//      if( diff > 0.0 ) rhomax = rho;
//      else if( diff < 0.0 ) rhomin = rho;
//      else break;
//      rho = 0.5*(rhomin+rhomax);
//          
//      if(counter > maxcount)
//      {
//          cerr.precision(15);
//          
//          if(rho0 < 322.)
//          {
//              cerr << counter << "\t" << diff << "\t" << Pressure( rho, TK ) << "\t" << psat << endl;
//              cerr << "\trhosatv = " << VaporSaturationDensity( TK ) << ",\trho = " << rho << endl;
//          }
//          break;
//      }
//    }
//  

// }


//
//double IAPWS95_TP_Interface::MolarVolume( const double& TK, const double& Ppa, const double& rho0 )
//{
//    return DensityAndX2MolarVolume (Density( TK, Ppa, rho0 ), 0.0);
//}
