#include <cmath>
#include <limits>
#include <iostream>


#include "iapws95_eos_idealgaspart.h"
#include "iapws95_critical_constants.h"
#include "iapws95_eos_idealgaspart.h"

using namespace std;

IAPWS95_IdealGasPart::IAPWS95_IdealGasPart()
  :
  tcrit(criticalTemperatureInKelvin),
  rhocrit(criticalDensity),
  ni0({{-8.3204464837497,
          6.6832105275932,
          3.00632,
          0.012436,
          0.97315,
          1.27950,
          0.96956,
          0.24873
          }}),
  
  gamma0_i({{
        std::numeric_limits<double>::quiet_NaN(),
          std::numeric_limits<double>::quiet_NaN(),
          std::numeric_limits<double>::quiet_NaN(),
          1.28728967,
          3.53734222,
          7.74073708,
          9.24437796,
          27.5075105 
          }})
{

}
  
IAPWS95_IdealGasPart::~IAPWS95_IdealGasPart()
{
}


double IAPWS95_IdealGasPart::NondimensionalIdealGasPart(const double& rho, const double& TK)
{
  const double delta{ rho/rhocrit };
  const double tau{ tcrit/TK };
  double ideal{log(delta)};
  ideal += ni0[0];
  ideal += ni0[1]*tau;
  ideal += ni0[2]*log(tau);
  for(int i = 3; i < 8; ++i)
    ideal += ni0[i]*log( 1.0 - exp(-gamma0_i[i]*tau) );
  return ideal;
} 


double IAPWS95_IdealGasPart::NondimensionalDIdealGasPartDtau(const double& TK)
{
  const double tau{ tcrit/TK };
  double didealdtau(0.0);
  didealdtau += ni0[1];
  didealdtau += ni0[2]/tau;
  for(int i = 3; i < 8; ++i)
    didealdtau += ni0[i]*gamma0_i[i]*( 1.0/(1.0-exp(-gamma0_i[i]*tau)) -1.0 );
  return didealdtau;
} 


double IAPWS95_IdealGasPart::NondimensionalD2IdealGasPartDtau2(const double& TK)
{
  const double tau{ tcrit/TK };
  double d2idealdtau2{-ni0[2]/tau/tau};
  for(int i=3; i <8; ++i)
    d2idealdtau2 -= ni0[i]*gamma0_i[i]*gamma0_i[i]*exp(-gamma0_i[i]*tau)/(1.0-exp(-gamma0_i[i]*tau))/(1.0-exp(-gamma0_i[i]*tau));
  return d2idealdtau2;
}





