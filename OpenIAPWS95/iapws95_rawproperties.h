/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   iapws95_rawproperties.h
 * Author: td
 *
 * Created on October 4, 2018, 10:41 AM
 */

#include "iapws95_eos_idealgaspart.h"
#include "iapws95_eos_residualpart.h"

#ifndef IAPWS95_RAWPROPERTIES_H
#define IAPWS95_RAWPROPERTIES_H

class IAPWS95_Rawproperties
{
 
private :
    const double  r;  // kJ kg-1 K-1
    
    IAPWS95_IdealGasPart  eos_idealgaspart;
    IAPWS95_ResidualPart  eos_residualpart;

public :
        
  IAPWS95_Rawproperties();
    
  // 5. Thermodynamic properties
  double Pressure( const double& rho, const double& TK );
  double InternalEnergy( const double& rho, const double& TK );
  double Entropy( const double& rho, const double& TK );
  double Enthalpy( const double& rho, const double& TK );
  double IsochoricHeatCapacity( const double& rho, const double& TK );
  double IsobaricHeatCapacity( const double& rho, const double& TK );
  double Density( const double& TK, const double& Ppa, const double& rho0 = 500. );
  double MolarVolume( const double& TK, const double& Ppa, const double& rho0 = 500. );

};
#endif /* IAPWS95_RAWPROPERTIES_H */

