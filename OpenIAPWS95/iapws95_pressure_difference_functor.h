/*
 * Copyright (C) 2018 td
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   iapws95_pressure_difference_functor.h
 * Author: td
 *
 * Created on October 21, 2018, 3:43 PM
 */

#ifndef IAPWS95_PRESSURE_DIFFERENCE_FUNCTOR_H
#define IAPWS95_PRESSURE_DIFFERENCE_FUNCTOR_H

#include "iapws95_rawproperties.h"

class IAPWS95_PressureDifferenceFunctor{
public:
    //IAPWS95_PressureDifferenceFunctor() = delete;
    IAPWS95_PressureDifferenceFunctor(const double& tKelvin, const double& targetPressure);
    double operator()(double density_estimate );
private:
    const double& tKelvin; 
    const double& targetPressure;
    IAPWS95_Rawproperties rawproperties;
};

#endif /* IAPWS95_PRESSURE_DIFFERENCE_FUNCTOR_H */

