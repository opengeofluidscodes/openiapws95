/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   iapws95_auxfunctions.h
 * Author: td
 *
 * Created on October 4, 2018, 9:26 AM
 */

#ifndef IAPWS95_AUXFUNCTIONS_H
#define IAPWS95_AUXFUNCTIONS_H

  double EstimateSaturationLiquidDensity( const double& TK );
  double EstimateSaturationVaporDensity( const double& TK );
  double EstimateSaturationPressure( const double& TK );

#endif /* IAPWS95_AUXFUNCTIONS_H */

