/*
 * Copyright (C) 2018 td
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "iapws95_pressure_difference_functor.h"
#include "iapws95_rawproperties.h"


IAPWS95_PressureDifferenceFunctor::IAPWS95_PressureDifferenceFunctor(const double& tKelvin, const double& targetPressure)
:
tKelvin(tKelvin),
targetPressure(targetPressure)
{
    
}


double IAPWS95_PressureDifferenceFunctor::operator()( double density_estimate )
{
    return rawproperties.Pressure( density_estimate, tKelvin ) - targetPressure;
}