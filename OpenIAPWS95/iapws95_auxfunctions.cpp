/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include<cmath>

#include "iapws95_auxfunctions.h"
#include "iapws95_critical_constants.h"

double EstimateSaturationPressure( const double& TK )
{
  const double pc = 22.06400000000213e6/*22.064e6*/; // caution: is this consistent???
  const double a1 = -7.85951783; 
  const double a2 = 1.84408259;
  const double a3 = -11.7866497;
  const double a4 = 22.6807411;
  const double a5 = -15.9618719;
  const double a6 = 1.80122502;

  double theta(1.0-TK/criticalTemperatureInKelvin);
  double result = 
    + a1*theta
    + a2*pow(theta,1.5) 
    + a3*pow(theta,3)
    + a4*pow(theta,3.5)
    + a5*pow(theta,4)
    + a6*pow(theta,7.5);
  result *= criticalTemperatureInKelvin/TK;
  result  = exp(result);
  return result * pc;

}

double EstimateSaturationLiquidDensity( const double& TK )
{
  const double b1(1.99274064);
  const double b2(1.09965342);
  const double b3(-0.510839303);
  const double b4(-1.75493479);
  const double b5(-45.5170352);
  const double b6(-6.74694450e5);
  double theta(1.0-TK/criticalTemperatureInKelvin);
  double mytheta(pow(theta,1./3.));

  double result = 1.0 
    + b1*mytheta 
    + b2*pow(mytheta,2) 
    + b3*pow(mytheta,5)
    + b4*pow(mytheta,16)
    + b5*pow(mytheta,43)
    + b6*pow(mytheta,110);
  return result*criticalDensity;
} 

double EstimateSaturationVaporDensity( const double& TK )
{
  const double c1(-2.03150240);
  const double c2(-2.68302940);
  const double c3(-5.38626492);
  const double c4(-17.2991605);
  const double c5(-44.7586581);
  const double c6(-63.9201063);

  double theta(1.0-TK/criticalTemperatureInKelvin);
  double mytheta(pow(theta,1./6.));

  double result = 
    + c1*pow(mytheta,2)
    + c2*pow(mytheta,4) 
    + c3*pow(mytheta,8)
    + c4*pow(mytheta,18)
    + c5*pow(mytheta,37)
    + c6*pow(mytheta,71);
  result = exp(result);
  return result*criticalDensity;
}