#ifndef IAPWS95_H
#define IAPWS95_H

#include<array>

class IAPWS95_IdealGasPart{

 private:

     const double tcrit;
     const double rhocrit;
  const std::array<double,8>  ni0;
  const std::array<double,8>  gamma0_i;

    
 public:

  IAPWS95_IdealGasPart();
  ~IAPWS95_IdealGasPart();
  
  double NondimensionalIdealGasPart(const double& rho, const double& TK);
  double NondimensionalDIdealGasPartDtau(const double& TK);  
  double NondimensionalD2IdealGasPartDtau2(const double& TK);
  
};

#endif
