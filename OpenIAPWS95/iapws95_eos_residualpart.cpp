/*
 * Copyright (C) 2018 td
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include<cmath>
#include<limits>

#include "iapws95_critical_constants.h"
#include "iapws95_eos_residualpart.h"

using namespace std;

IAPWS95_ResidualPart::IAPWS95_ResidualPart()
  :
  tcrit(criticalTemperatureInKelvin),
  rhocrit(criticalDensity),
  c({{    std::numeric_limits<int>::quiet_NaN(),
          std::numeric_limits<int>::quiet_NaN(),
          std::numeric_limits<int>::quiet_NaN(),
          std::numeric_limits<int>::quiet_NaN(),
          std::numeric_limits<int>::quiet_NaN(),
          std::numeric_limits<int>::quiet_NaN(),
          std::numeric_limits<int>::quiet_NaN(),
          1, 1, 1, 1, 1,
          1, 1, 1, 1, 1,
          1, 1, 1, 1, 1,
          2, 2, 2, 2, 2,
          2, 2, 2, 2, 2,
          2, 2, 2, 2, 2,
          2, 2, 2, 2, 2,
          3, 3, 3, 3, 4,
          6, 6, 6, 6
          }}),

  d({{    1,  1,  1,  2,  2,
          3,  4,  1,  1,  1,
          2,  2,  3,  4,  4,
          5,  7,  9, 10, 11,
          13, 15,  1,  2,  2,
          2,  3,  4,  4,  4,
          5,  6,  6,  7,  9, 
          9,  9,  9,  9, 10, 
          10, 12,  3,  4,  4,
          5, 14,  3,  6,  6, 
          6,  3,  3,  3
          }}),

  t({{   -0.5,     0.875,     1.0,      0.5,       0.75,
          0.375,   1.0,       4.0,      6.0,      12.0,
          1.0,     5.0,       4.0,      2.0,      13.0,
          9.0,     3.0,       4.0,     11.0,       4.0,
          13.0,    1.0,       7.0,      1.0,       9.0,
          10.0,   10.0,       3.0,      7.0,      10.0,
          10.0,    6.0,      10.0,     10.0,       1.0,
          2.0,     3.0,       4.0,      8.0,       6.0,
          9.0,     8.0,      16.0,     22.0,      23.0,
          23.0,   10.0,      50.0,     44.0,      46.0,
          50.0,    0.0,       1.0,      4.0
          }}),

  n({{    0.12533547935523e-1,   0.78957634722828e1,  -0.87803203303561e1,   0.31802509345418,     -0.26145533859358,
          -0.78199751687981e-2,   0.88089493102134e-2, -0.66856572307965,     0.20433810950965,     -0.66212605039687e-4,
          -0.19232721156002,     -0.25709043003438,     0.16074868486251,    -0.40092828925807e-1,   0.39343422603254e-6,
          -0.75941377088144e-5,   0.56250979351888e-3, -0.15608652257135e-4,  0.11537996422951e-8,   0.36582165144204e-6,
          -0.13251180074668e-11, -0.62639586912454e-9, -0.10793600908932,     0.17611491008752e-1,   0.22132295167546,
          -0.40247669763528,      0.58083399985759,     0.49969146990806e-2, -0.31358700712549e-1,  -0.74315929710341,
          0.47807329915480,      0.20527940895948e-1, -0.13636435110343,     0.14180634400617e-1,   0.83326504880713e-2,
          -0.29052336009585e-1,   0.38615085574206e-1, -0.20393486513704e-1, -0.16554050063734e-2,   0.19955571979541e-2,
          0.15870308324157e-3,  -0.16388568342530e-4,  0.43613615723811e-1,  0.34994005463765e-1,  -0.76788197844621e-1,
          0.22446277332006e-1,  -0.62689710414685e-4, -0.55711118565645e-9, -0.19905718354408,      0.31777497330738,
          -0.11841182425981,     -0.31306260323435e2,   0.31546140237781e2,  -0.25213154341695e4,   -0.14874640856724,
          0.31806110878444
          }}),

  alfa({{ 20.0, 20.0, 20.0 }}),
  beta({{ 150.0, 150.0, 250.0, 0.3, 0.3 }}),
  gamma({{ 1.21, 1.21, 1.25 }}),
  epsilon({{ 1.0, 1.0, 1.0 }}),
  C({{ 28.0, 32.0 }}),
  D({{ 700.0, 800.0 }}),
  A({{ 0.32, 0.32 }}),
  B({{ 0.2, 0.2 }}),
  b({{ 0.85, 0.95 }}),
  a({{ 3.5, 3.5 }})
  {
      
  }
  
IAPWS95_ResidualPart::~IAPWS95_ResidualPart()
{
    
}


double IAPWS95_ResidualPart::NondimensionalResidualPart(const double& rho, const double& TK)
{
  const double delta{ rho/rhocrit };
  const double tau{ tcrit/TK };
  double residual{0.};
  for(int i = 0; i < 7; ++i)
    residual += n[i]*pow(delta,d[i])*pow(tau,t[i]);

  for(int i = 7; i < 51; ++i)
    residual += n[i]*pow(delta,d[i])*pow(tau,t[i])*exp(-pow(delta,c[i]));

  for(int i = 51; i < 54; ++i)
    {
      int k(i-51); // k only for alfa, beta, gamma, epsilon
      residual += n[i] 
        * pow(delta,d[i]) 
        * pow(tau,t[i]) 
        * exp( -alfa[k]*(delta-epsilon[k])*(delta-epsilon[k]) - beta[k]*(tau-gamma[k])*(tau-gamma[k]) );
    }

  for(int i = 54; i < 56; ++i)
    {
      int k(i-54), j(i-51);
      double delta_min1_sq = (delta-1.0)*(delta-1.0);
      double Theta = 1.0-tau + A[k]*pow( delta_min1_sq, 0.5/beta[j] );
      double Delta = Theta*Theta + B[k]*pow( delta_min1_sq, a[k] );
      double Psi   = exp( -C[k]*delta_min1_sq - D[k]*(tau-1.0)*(tau-1.0) );
      residual += n[i]*pow( Delta, b[k] )*delta*Psi;
    }
  return residual;
}

double IAPWS95_ResidualPart::NondimensionalDResidualPartDdelta(const double& rho, const double& TK)
{
  const double delta{ rho/rhocrit };
  const double tau{ tcrit/TK };
  double dresidualddelta{0.};

  for(int i = 0; i < 7; ++i)
    dresidualddelta += n[i]*d[i]*pow(delta,d[i]-1)*pow(tau,t[i]);

  for(int i = 7; i < 51; ++i)
    dresidualddelta += n[i] * exp(-pow(delta,c[i])) * (  pow(delta,d[i]-1)
                                                         * pow(tau,t[i])
                                                         * (static_cast<double>(d[i])-static_cast<double>(c[i])*pow(delta,c[i])) 
                                                         );
  
  for(int i = 51; i < 54; ++i)
    {
      int k(i-51); // k only for alfa, beta, gamma, epsilon
      dresidualddelta += n[i] * pow(delta,d[i]) * pow(tau,t[i]) 
        * exp( -alfa[k]*(delta-epsilon[k])*(delta-epsilon[k]) - beta[k]*(tau-gamma[k])*(tau-gamma[k]) )
        * ( d[i]/delta-2.0*alfa[k]*(delta-epsilon[k]) );
    }
  for(int i = 54; i < 56; ++i)
    {
      int k(i-54), j(i-51);// k only for alfa, beta, epsilon; j for beta
      double delta_min1_sq = (delta-1.0)*(delta-1.0);
      double Theta = 1.0-tau + A[k]*pow( delta_min1_sq, 0.5/beta[j] );
      double Delta = Theta*Theta + B[k]*pow( delta_min1_sq, a[k] );
      double Psi   = exp( -C[k]*delta_min1_sq - D[k]*(tau-1.0)*(tau-1.0) );
      double dPsiddelta = -2.0*C[k]*(delta-1.0)*Psi;
      double dDeltaddelta = (delta-1.0) * ( A[k]*Theta*2.0/beta[j]*pow( delta_min1_sq, 0.5/beta[j]-1.0 )
                                            + 2.0*B[k]*a[k]*pow( delta_min1_sq, a[k]-1.0 ) );
      double dDeltaddeltabi = 0.; 
      if(fabs(1.0-delta)>2.0*numeric_limits<double>::epsilon()) // not at critical density; else term is zero
        dDeltaddeltabi = b[k]*pow(Delta,b[k]-1.0)*dDeltaddelta; 
      dresidualddelta += n[i]*( pow( Delta, b[k] )*( Psi+delta*dPsiddelta) + dDeltaddeltabi*delta*Psi );
    }
  return dresidualddelta;
}


double IAPWS95_ResidualPart::NondimensionalDResidualPartDtau(const double& rho, const double& TK)
{
  const double delta{ rho/rhocrit };
  const double tau{ tcrit/TK };
  double dresidualdtau{0.};
  for(int i = 0; i < 7; ++i)
    dresidualdtau += n[i]*t[i]*pow(delta,d[i])*pow(tau,t[i]-1.0);

  for(int i = 7; i < 51; ++i)
    dresidualdtau += n[i]*t[i]*pow(delta,d[i])*pow(tau,t[i]-1.0)*exp(-pow(delta,c[i]));

  for(int i = 51; i < 54; ++i)
    {
      int k(i-51);// k only for alfa, beta, gamma, epsilon
      dresidualdtau += n[i] 
        * pow(delta,d[i]) 
        * pow(tau,t[i]) 
        * exp( -alfa[k]*(delta-epsilon[k])*(delta-epsilon[k]) - beta[k]*(tau-gamma[k])*(tau-gamma[k]) )
        * (t[i]/tau-2.0*beta[k]*(tau-gamma[k]));
    }

  for(int i = 54; i < 56; ++i)
    {
      int k(i-54), j(i-51);// k only for alfa, beta, epsilon; j for beta
      /* TD, 19.7.18: at the critical temperature or density, 1-tau or delta-1 are zero. 
         This can cause problems with the pow function. Therefore the following if-else.
      */
      if( ( fabs(1.0-tau)>2.0*numeric_limits<double>::epsilon() )
              &&
          ( fabs(1.0-delta)>2.0*numeric_limits<double>::epsilon() ) 
        ) // normal situation: NOT at critical point
      {
          double delta_min1_sq = (delta-1.0)*(delta-1.0);
          double Theta = 1.0-tau + A[k]*pow( delta_min1_sq, 0.5/beta[j] ); // Theta is zero
          double Delta = Theta*Theta + B[k]*pow( delta_min1_sq, a[k] );
          double Psi   = exp( -C[k]*delta_min1_sq - D[k]*(tau-1.0)*(tau-1.0) );
          double dDeltadtaubi = -2.0 * Theta * b[k] * pow(Delta,b[k]-1.0);
          double DPsidtau = -2.0 * D[k]*(tau-1.0) * Psi;
          dresidualdtau += n[i] * delta * ( dDeltadtaubi*Psi + pow(Delta,b[k])*DPsidtau );   
      }
      else if( ( fabs(1.0-tau)<2.0*numeric_limits<double>::epsilon() )
                   &&
               ( fabs(1.0-delta)<2.0*numeric_limits<double>::epsilon() ) 
             ) // essentially at critical point
      {
          dresidualdtau += 0.0;
      }
      else if(( fabs(1.0-tau)<2.0*numeric_limits<double>::epsilon() )
                   &&
              ( fabs(1.0-delta)>2.0*numeric_limits<double>::epsilon() )
             ) // at the critical temperature but not at critical point
      {
          double delta_min1_sq = (delta-1.0)*(delta-1.0);
          double Theta = 0.0 + A[k]*pow( delta_min1_sq, 0.5/beta[j] );
          double Delta = Theta*Theta + B[k]*pow( delta_min1_sq, a[k] );
          double Psi   = exp( -C[k]*delta_min1_sq );
          double dDeltadtaubi = -2.0 * Theta * b[k] * pow(Delta,b[k]-1.0);
          dresidualdtau += n[i] * delta * dDeltadtaubi*Psi;
      }
      else if(( fabs(1.0-tau)>2.0*numeric_limits<double>::epsilon() )
                   &&
              ( fabs(1.0-delta)<2.0*numeric_limits<double>::epsilon() )
             ) // at the critical density but not at critical point
      {
          double Theta = 1.0-tau;
          double Delta = Theta*Theta;
          double Psi   = exp( - D[k]*(tau-1.0)*(tau-1.0) );
          double dDeltadtaubi = -2.0 * Theta * b[k] * pow(Delta,b[k]-1.0);
          double DPsidtau = -2.0 * D[k]*(tau-1.0) * Psi;
          dresidualdtau += n[i] * delta * ( dDeltadtaubi*Psi + pow(Delta,b[k])*DPsidtau );  
      }
    }

  return dresidualdtau;
}


double IAPWS95_ResidualPart::NondimensionalD2ResidualPartDdelta2(const double& rho, const double& TK)
{
  const double delta{ rho/rhocrit };
  const double tau{ tcrit/TK };
  double d2residualddelta2{0.};

  for(int i = 0; i < 7; ++i)
    d2residualddelta2 += n[i] * static_cast<double>(d[i]) * static_cast<double>((d[i]-1)) * pow(delta,d[i]-2) * pow(tau,t[i]);

  for(int i = 7; i < 51; ++i)
    d2residualddelta2 += n[i] * exp(-pow(delta,c[i])) 
      * (  pow(delta,d[i]-2) * pow(tau,t[i]) 
           * ( (static_cast<double>(d[i])-static_cast<double>(c[i])*pow(delta,c[i]))
               * (static_cast<double>(d[i])-1.0-static_cast<double>(c[i])*pow(delta,c[i]))
               -static_cast<double>(c[i]*c[i])*pow(delta,c[i])
               )
           );

  for(int i = 51; i < 54; ++i)
    {
      int k(i-51);
      d2residualddelta2 += n[i] * pow(tau,t[i]) * exp( -alfa[k]*(delta-epsilon[k])*(delta-epsilon[k]) - beta[k]*(tau-gamma[k])*(tau-gamma[k]) )
        * ( pow(delta,d[i])*(-2.0*alfa[k]+4.0*alfa[k]*alfa[k]*(delta-epsilon[k])*(delta-epsilon[k]))
            - 4.0*static_cast<double>(d[i])*alfa[k]*pow(delta,d[i]-1)*(delta-epsilon[k])+static_cast<double>(d[i]*(d[i]-1))*pow(delta,d[i]-2));
    }

  for(int i = 54; i < 56; ++i)
    {
      int k(i-54), j(i-51);
      double delta_min1_sq = (delta-1.0)*(delta-1.0); 
      double Theta = 1.0-tau + A[k]*pow( delta_min1_sq, 0.5/beta[j] ); 
      double Delta = Theta*Theta + B[k]*pow( delta_min1_sq, a[k] ); 

      double Psi   = exp( -C[k]*delta_min1_sq - D[k]*(tau-1.0)*(tau-1.0) ); 
      double dPsiddelta = -2.0*C[k]*(delta-1.0)*Psi; 
      double d2Psiddelta2 = (2.0*C[k]*delta_min1_sq-1.0)*2.0*C[k]*Psi; 
      double dDeltaddelta = (delta-1.0) * ( A[k]*Theta*2.0/beta[j]*pow( delta_min1_sq, 0.5/beta[j]-1.0 )
                                            + 2.0*B[k]*a[k]*pow( delta_min1_sq, a[k]-1.0 ) ); 
      double dDeltaddeltabi = b[k]*pow(Delta,b[k]-1.0)*dDeltaddelta;
      double d2Deltaddelta2 = 1./(delta-1.0)*dDeltaddelta 
        + delta_min1_sq * ( 4.0 * B[k] * a[k] * (a[k]-1.0) * pow(delta_min1_sq,a[k]-2.0)
                            + 2.0*A[k]*A[k]/beta[j]/beta[j]* pow(delta_min1_sq,0.5/beta[j]-1.0)* pow(delta_min1_sq,0.5/beta[j]-1.0)
                            + A[k]*Theta*4.0/beta[j]*(0.5/beta[j]-1.0)* pow(delta_min1_sq,0.5/beta[j]-2.0) ); 

      double d2Deltabiddelta2 = b[k] * ( pow(Delta,b[k]-1.0) * d2Deltaddelta2 + (b[k]-1.0)*pow(Delta,b[k]-2.0)*dDeltaddelta*dDeltaddelta ); 

      d2residualddelta2 += n[i] * ( pow(Delta,b[k]) 
                                    * ( 2.0*dPsiddelta + delta*d2Psiddelta2 ) 
                                    + 2.0*dDeltaddeltabi * (Psi+delta* dPsiddelta)
                                    + d2Deltabiddelta2*delta*Psi );
    }
  return d2residualddelta2;
}


double IAPWS95_ResidualPart::NondimensionalD2ResidualPartDtau2(const double& rho, const double& TK)
{
  const double delta{ rho/rhocrit };
  const double tau{ tcrit/TK };
  double d2residualdtau2{0.};

  for(int i = 0; i < 7; ++i)
    d2residualdtau2 += n[i]*t[i]*(t[i]-1.0)*pow(delta,d[i])*pow(tau,t[i]-2.0);

  for(int i = 7; i < 51; ++i)
    d2residualdtau2 += n[i]*t[i]*(t[i]-1.0)*pow(delta,d[i])*pow(tau,t[i]-2.0)*exp(-pow(delta,c[i]));

  for(int i = 51; i < 54; ++i)
    {
      int k(i-51);
      d2residualdtau2 += n[i] 
        * pow(delta,d[i]) 
        * pow(tau,t[i]) 
        * exp( -alfa[k]*(delta-epsilon[k])*(delta-epsilon[k]) - beta[k]*(tau-gamma[k])*(tau-gamma[k]) )
        * ( (t[i]/tau-2.0*beta[k]*(tau-gamma[k]))*(t[i]/tau-2.0*beta[k]*(tau-gamma[k])) - t[i]/tau/tau - 2.0*beta[k]) ;
    }

  for(int i = 54; i < 56; ++i)
    {
      int k(i-54), j(i-51);
      double delta_min1_sq = (delta-1.0)*(delta-1.0);
      double Theta = 1.0-tau + A[k]*pow( delta_min1_sq, 0.5/beta[j] );
      double Delta = Theta*Theta + B[k]*pow( delta_min1_sq, a[k] );
      double Psi   = exp( -C[k]*delta_min1_sq - D[k]*(tau-1.0)*(tau-1.0) );
      double dDeltadtaubi = -2.0 * Theta * b[k] * pow(Delta,b[k]-1.0);
      double DPsidtau = -2.0 * D[k]*(tau-1.0) * Psi;
      double D2Psidtau2 = (2.0*D[k]*(tau-1.0)*(tau-1.0)-1.0)*2.0*D[k]*Psi;
      double d2Deltadtaubi2 = 2.0*b[k]*pow(Delta,b[k]-1.0) + 4.0*Theta*Theta*b[k]*(b[k]-1.0)*pow(Delta,b[k]-2.0);
      d2residualdtau2 += n[i]*delta*(Psi*d2Deltadtaubi2 + 2.0*dDeltadtaubi*DPsidtau + pow(Delta,b[k])*D2Psidtau2 );
    }
  return d2residualdtau2;
}


double IAPWS95_ResidualPart::NondimensionalDResidualPartDdeltaDtau(const double& rho, const double& TK)
{
  const double delta{ rho/rhocrit };
  const double tau{ tcrit/TK };
  double dresidualddeltadtau{0.};

  for(int i = 0; i < 7; ++i)
    dresidualddeltadtau += n[i]*d[i]*t[i]*pow(delta,d[i]-1.0)*pow(tau,t[i]-1.0);

  for(int i = 7; i < 51; ++i)
    dresidualddeltadtau += n[i]*t[i]*pow(delta,d[i]-1.0)*pow(tau,t[i]-1.0)*(d[i]-c[i]*pow(delta,c[i]))*exp(-pow(delta,c[i]));

  for(int i = 51; i < 54; ++i)
    {
      int k(i-51);
      dresidualddeltadtau += n[i] 
        * pow(delta,d[i]) 
        * pow(tau,t[i]) 
        * exp( -alfa[k]*(delta-epsilon[k])*(delta-epsilon[k]) - beta[k]*(tau-gamma[k])*(tau-gamma[k]) )
        * ( d[i]/delta-2.0*alfa[k]*(delta-epsilon[k]) )
        * ( t[i]/tau  -2.0*beta[k]*(tau-gamma[k]) );
    }

  for(int i = 54; i < 56; ++i)
    {
      int k(i-54), j(i-51);
      double delta_min1_sq = (delta-1.0)*(delta-1.0);
      double Theta = 1.0-tau + A[k]*pow( delta_min1_sq, 0.5/beta[j] );
      double Delta = Theta*Theta + B[k]*pow( delta_min1_sq, a[k] );
      double Psi   = exp( -C[k]*delta_min1_sq - D[k]*(tau-1.0)*(tau-1.0) );
      double dDeltadtaubi = -2.0 * Theta * b[k] * pow(Delta,b[k]-1.0);
      double dPsidtau = -2.0 * D[k]*(tau-1.0) * Psi;
      double dPsiddelta = -2.0*C[k]*(delta-1.0)*Psi;
      double d2Psiddeltadtau = 4.0*C[k]*D[k]*(delta-1.0)*(tau-1.0)*Psi;
      double dDeltaddelta = (delta-1.0) * ( A[k]*Theta*2.0/beta[j]*pow( delta_min1_sq, 0.5/beta[j]-1.0 )
                                            + 2.0*B[k]*a[k]*pow( delta_min1_sq, a[k]-1.0 ) );
      double dDeltabiddelta = b[k]*pow(Delta,b[k]-1.0)*dDeltaddelta;
      double d2deltabiddeltadtau = 
        -A[k] * b[k] * 2.0/beta[j] * pow(Delta,b[k]-1.0) * (delta-1.0) * pow( (delta-1.0)*(delta-1.0), 0.5/beta[j]-1.0 )
        - 2.0 * Theta * b[k] * (b[k]-1.0) * pow(Delta,b[k]-2.0) * dDeltaddelta;

      dresidualddeltadtau += 
        n[i]
        *(pow(Delta,b[k])
          * ( dPsidtau + delta*d2Psiddeltadtau )
          + delta*dDeltabiddelta*dPsidtau
          + dDeltadtaubi*(Psi+delta*dPsiddelta)
          + d2deltabiddeltadtau*delta*Psi );
    }
  return dresidualddeltadtau;
}
