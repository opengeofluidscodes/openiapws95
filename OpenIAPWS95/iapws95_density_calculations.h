/*
 * Copyright (C) 2018 td
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   iapws95_density_calculations.h
 * Author: td
 *
 * Created on October 21, 2018, 12:24 PM
 */

#ifndef IAPWS95_DENSITY_CALCULATIONS_H
#define IAPWS95_DENSITY_CALCULATIONS_H

#include "iapws95_rawproperties.h"
#include "iapws95_eos_residualpart.h"
#include "iapws95_pressure_difference_functor.h"

class IAPWS95_DensityCalculations{

public :
    IAPWS95_DensityCalculations( const double& tKelvin, const double& pPascal );
    double DensityFromTP();
    
private :
    IAPWS95_ResidualPart eos_residualpart;
    const  double& tKelvin;
    const  double& pPascal;
    const  double  r;  // kJ kg-1 K-1
    double rho0;
    double bracket_factor;
    void   FindInitialDensityAndBrackets();
};
    

#endif /* IAPWS95_DENSITY_CALCULATIONS_H */

