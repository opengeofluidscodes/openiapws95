/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   iapws95_eos.h
 * Author: td
 *
 * Created on October 4, 2018, 11:17 AM
 */
#include "iapws95_rawproperties.h"
#include "iapws95_saturation.h"
#include "iapws95_density_calculations.h"


#ifndef IAPWS95_TP_INTERFACE_H
#define IAPWS95_TP_INTERFACE_H

class IAPWS95_TP_Interface
{
    // constructors
    // - disable default

private :
    
    const double& tKelvin;
    const double& pPascal;
    
    IAPWS95_Rawproperties raw;
    IAPWS95_DensityCalculations density;
//    IAPWS95_Saturation    sat;
        
public :
    
    IAPWS95_TP_Interface( const double& tKelvin, const double& pPascal );

    double Density();
    double InternalEnergy();
    double Entropy();
    double Enthalpy();
    double IsochoricHeatCapacity();
    double IsobaricHeatCapacity();
    double MolarVolume();
    
    double CriticalTemperature();
    double CriticalDensity();    

};

#endif /* IAPWS95_TP_INTERFACE_H */

