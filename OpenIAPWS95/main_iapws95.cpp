#include <iostream>
#include <fstream>
#include <cmath>
#include "iapws95_density_calculations.h"
#include "iapws95_rawproperties.h"

using namespace std;

int main()
{
  
//  kestin84 kestin;
  double p, t, TK,/*[11]={300., 350., 400., 450., 500., 550., 600., 650., 700., 750., 800.}*/
    rho(838.025), ratio, ripfr_l, ripfr_v;
  int i(1);

  IAPWS95_DensityCalculations  eos(TK,p);
  IAPWS95_Rawproperties        raw;
  while(i>0)
  {
  cout << "T in C  :"; cin >> t;
  TK = t+273.15;
  cout << "P in Pa :"; cin >> p;
  rho = eos.DensityFromTP();
  cout << "Entropy = " << raw.Entropy(rho,TK) << endl;
  }

  // ofstream ofile("psat_vs_1e6overT2.dat");
  // for( TK = 278.15; TK < 633.16; TK += 1.)
  //   ofile << 1.0e6/TK/TK << "\t" << eos.SaturationPressure( TK ) << endl;
  // for( TK = 634.15; TK < 647.09; TK += 0.1)
  //   ofile << 1.0e6/TK/TK << "\t" << eos.SaturationPressure( TK ) << endl;
    

  // ofstream ofile("rho-p.dat");
                 
  // for(int i =0; i < 11; ++i)
  //   {
  //     for( rho = 100.; rho < 1050.; rho += 1.)
  //       {
  //         if(rho < eos.EstimateSaturationLiquidDensity( TK[i] )*1.01) continue;
  //         ofile << rho << "\t" << eos.Pressure(rho,TK[i])*1.0e-5 << endl;
  //       }
  //     ofile << endl;
  //   }


  //"\t" << eos.D2ResidualPartDtau2(rho,TK) << "\t" << eos.IsochoricHeatCapacity(rho,TK) << endl;

  // while(i>0)
  //   {
  //     cout << "Please enter T in K       : "; cin >> TK;
  //     cout << "Please enter rho in kg/m3 : "; cin >> rho;
  //     cout << "Pressure : " 
  //          << kestin.PressureFromDimensionalInput( rho, TK ) << "\t"
  //          << kestin.Pressure( rho/kestin.Rhostar(), TK/kestin.Tstar() ) << endl;
  //   }

  // ofstream ofile("polyakov_vl.dat");
  // for(double TK = 298.15; TK < 647.0; TK += 1.)
  //   {
  //     p   = eos.SaturationPressure( TK );
  //     double   rhol = eos.LiquidSaturationDensity( TK ); 
  //     double   rhov = eos.VaporSaturationDensity( TK ); 
  //     double   rhodl(rhol*20.023/18.01);
  //     double   rhodv(rhov*20.023/18.01);
  //     double   tau(eos.Tcrit()/TK);
  //     double   deltal(rhol/eos.Rhocrit());
  //     double   deltav(rhov/eos.Rhocrit());
      
  //     const double D(0.226454);
  //     const double omega(0.344290);
  //     const double omegad(0.364438);
  //     const double Dtcrit(0.3207e-3);
  //     const double Drhocrit(1.471e-3);
  //     const double Domega(1.363e-3);
      
  //     double dphidomegal = (D*tau*kestin.ResidualPart(deltal,1./tau)-eos.ResidualPart( rhol, TK ))/(omegad-omega);
  //     double dphidomegav = (D*tau*kestin.ResidualPart(deltav,1./tau)-eos.ResidualPart( rhov, TK ))/(omegad-omega);
      
  //     double lna = tau * (eos.DResidualPartDtau(rhov,TK)-eos.DResidualPartDtau(rhol,TK))*Dtcrit;
  //     lna += (deltal*eos.DResidualPartDdelta( rhol, TK )-deltav*eos.DResidualPartDdelta( rhov, TK ))*Drhocrit;
  //     lna += omega*(dphidomegav - dphidomegal)*Domega;

  //     ofile << TK << "\t" << 1000.*lna << "\t" << -7.685+6.7123e3/TK - 1.6664e6/TK/TK +0.35041e9/TK/TK/TK << endl;
  //   }
}


//double Beta18O_H2O_Vapor_Rosenbaum1997( const double& TK )
//{
//  double invT2 = 1.0e6/TK/TK;
//  return -13.9403 * ( exp(-0.686991*invT2) - 1.0) + invT2 * (5.29887 - 0.0937653*invT2);
//}

